const MongoClient = require('mongodb').MongoClient;
const assert = require('assert');

// Connection URL
const url = 'mongodb://localhost:27017';

// Database Name
const dbName = 'project_iot';

var amqp = require('amqplib/callback_api');

amqp.connect('amqp://localhost', function(error0, connection) {
    if (error0) {
        throw error0;
    }
    connection.createChannel(function(error1, channel) {
        if (error1) {
            throw error1;
        }

        var queue = 'temperature';

        channel.assertQueue(queue, {
            durable: false
        });

        console.log(" [*] Waiting for messages in %s. To exit press CTRL+C", queue);

        channel.consume(queue, function(msg) {
            console.log(" [x] Received %s", msg.content.toString());
            insertTemperature = function(db, callback, msg) {
                // Get the documents collection
                const collection = db.collection('temperatures');
                // Insert some documents

                let json = JSON.parse(msg.content);

                collection.insertOne(
                    { time: json.time, degree: json.degree}
                    , function(err, result) {
                        assert.equal(err, null);
                        assert.equal(1, result.result.n);
                        console.log("Inserted temperature into the collection");
                        callback(result);
                    });
            }

            // Use connect method to connect to the server
            MongoClient.connect(url, function(err, client) {
                assert.equal(null, err);
                console.log("Connected successfully to server");

                const db = client.db(dbName);

                insertTemperature(db,function() {
                    client.close();
                }, msg);
            });
        }, {
            noAck: true
        });

    });
});