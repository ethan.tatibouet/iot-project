const mongoose = require('mongoose');

const schema = mongoose.Schema({
    localDate : Number,
    degree: String
})

// export du modèle
// le dernier paramètre correspond au nom exact de la collection
module.exports = mongoose.model('mes_temperatures', schema, 'temperatures');