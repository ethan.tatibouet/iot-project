const mongoose = require('mongoose');
const temp = require('./temp');

// Connection URL, on se connecte à la base de données voulue
const url = 'mongodb://localhost:27017/project_iot';

mongoose.connect(url, {useNewUrlParser : true})

var express = require('express')
var hostname = 'localhost';
var port = '3000';
var app = express();

app.use(function(req, res, next) {
    res.header("Access-Control-Allow-Origin", "*"); // update to match the domain you will make the request from
    next();
});

app.get('/data', async(req, res) => {
    const temps = await temp.find();
    await res.json(temps);
})


app.listen(port, hostname, function(){
    console.log('my api')
});

